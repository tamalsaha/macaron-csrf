module gitea.com/macaron/csrf

go 1.11

require (
	gitea.com/macaron/macaron v1.3.3-0.20190803174002-53e005ff4827
	gitea.com/macaron/session v0.0.0-20190805015136-fc944d37022f
	github.com/Unknwon/com v0.0.0-20190321035513-0fed4efef755
	github.com/gopherjs/gopherjs v0.0.0-20190430165422-3e4dfb77656c // indirect
	github.com/smartystreets/assertions v1.0.1 // indirect
	github.com/smartystreets/goconvey v0.0.0-20190731233626-505e41936337
)
